include_directories (
    ${PROJECT_SOURCE_DIR}/src
)

file(GLOB_RECURSE SOURCES
    *.cpp
)

add_library(${PROJECT_NAME}-ut_lib OBJECT
   ${SOURCES}
)

add_custom_target(${PROJECT_NAME}-ut-run
    COMMAND ${EXECUTABLE_OUTPUT_PATH}/${PROJECT_NAME}-ut
                --gtest_output=xml:${EXECUTABLE_OUTPUT_PATH}/${PROJECT_NAME}-ut.xml
                --gtest_filter=${TESTCASES}
    DEPENDS ${PROJECT_NAME}-ut
)

add_test(
    NAME    ${PROJECT_NAME}-ut
    COMMAND ${PROJECT_NAME}-ut
                --gtest_output=xml:${EXECUTABLE_OUTPUT_PATH}/${PROJECT_NAME}-ut.xml
                --gtest_filter=${TESTCASES}
    DEPENDS ${PROJECT_NAME}-ut
)
